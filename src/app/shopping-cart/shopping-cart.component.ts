import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//model
import { Cart } from '../model/cart';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  @Input() arrCart: Cart[] = [];

  @Output() eventEmmitter: EventEmitter<[number, string]> = new EventEmitter<[number, string]>();

  constructor() { }

  ngOnInit(): void {
  }

  lessTickets(i: number){
    this.eventEmmitter.emit([i, "sell"]);
  }

  displayTitle(cart: Cart){
    return cart.arr_tickets.map(ticket=>ticket.amount > 0).includes(true);
  }

}
