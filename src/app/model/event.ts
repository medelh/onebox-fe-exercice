export class Event {
    //properties
    private _id: number;
    private _title: string;
    private _subtitle: string;
    private _image: string;
    private _place: string;
    private _startDate: Date;
    private _endDate: Date;
    private _description: string;


	constructor(id: number, title: string, subtitle: string, image: string, place: string, startDate: Date, endDate: Date, description: string) {
		this._id = id;
		this._title = title;
		this._subtitle = subtitle;
		this._image = image;
		this._place = place;
		this._startDate = startDate;
		this._endDate = endDate;
		this._description = description;
	}

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter title
     * @return {string}
     */
	public get title(): string {
		return this._title;
	}

    /**
     * Getter subtitle
     * @return {string}
     */
	public get subtitle(): string {
		return this._subtitle;
	}

    /**
     * Getter image
     * @return {string}
     */
	public get image(): string {
		return this._image;
	}

    /**
     * Getter place
     * @return {string}
     */
	public get place(): string {
		return this._place;
	}

    /**
     * Getter startDate
     * @return {Date}
     */
	public get startDate(): Date {
		return this._startDate;
	}

    /**
     * Getter endDate
     * @return {Date}
     */
	public get endDate(): Date {
		return this._endDate;
	}

    /**
     * Getter description
     * @return {string}
     */
	public get description(): string {
		return this._description;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter title
     * @param {string} value
     */
	public set title(value: string) {
		this._title = value;
	}

    /**
     * Setter subtitle
     * @param {string} value
     */
	public set subtitle(value: string) {
		this._subtitle = value;
	}

    /**
     * Setter image
     * @param {string} value
     */
	public set image(value: string) {
		this._image = value;
	}

    /**
     * Setter place
     * @param {string} value
     */
	public set place(value: string) {
		this._place = value;
	}

    /**
     * Setter startDate
     * @param {Date} value
     */
	public set startDate(value: Date) {
		this._startDate = value;
	}

    /**
     * Setter endDate
     * @param {Date} value
     */
	public set endDate(value: Date) {
		this._endDate = value;
	}

    /**
     * Setter description
     * @param {string} value
     */
	public set description(value: string) {
		this._description = value;
	}   

}