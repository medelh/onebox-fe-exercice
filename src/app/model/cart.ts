import { Event } from "./event";
import { Ticket } from './ticket';
export class Cart {
    //properties
    private _event: Event;
    private _arr_tickets: Ticket[];

	constructor(event: Event, arr_tickets: Ticket[]) {
		this._event = event;
		this._arr_tickets = arr_tickets;
	}

    /**
     * Getter event
     * @return {Event}
     */
	public get event(): Event {
		return this._event;
	}

    /**
     * Getter arr_tickets
     * @return {Ticket[]}
     */
	public get arr_tickets(): Ticket[] {
		return this._arr_tickets;
	}

    /**
     * Setter event
     * @param {Event} value
     */
	public set event(value: Event) {
		this._event = value;
	}

    /**
     * Setter arr_tickets
     * @param {Ticket[]} value
     */
	public set arr_tickets(value: Ticket[]) {
		this._arr_tickets = value;
	}

}