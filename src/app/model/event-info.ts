import { Event } from "./event";
import { Session } from "./session";
export class EventInfo {
    //properties
    private _event: Event;
    private _sessions: Session[];

    //constructor
	constructor(event: Event, sessions: Session[]) {
		this._event = event;
		this._sessions = sessions;
	}

    /**
     * Getter event
     * @return {Event}
     */
	public get event(): Event {
		return this._event;
	}

    /**
     * Getter sessions
     * @return {Session[]}
     */
	public get sessions(): Session[] {
		return this._sessions;
	}

    /**
     * Setter event
     * @param {Event} value
     */
	public set event(value: Event) {
		this._event = value;
	}

    /**
     * Setter sessions
     * @param {Session[]} value
     */
	public set sessions(value: Session[]) {
		this._sessions = value;
	}
}