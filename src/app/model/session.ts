export class Session {
    //properties
    private _date: Date;
    private _availability: number;

    //constructor
	constructor(date: Date, availability: number) {
		this._date = date;
		this._availability = availability;
	}

    /**
     * Getter date
     * @return {Date}
     */
	public get date(): Date {
		return this._date;
	}

    /**
     * Getter availability
     * @return {number}
     */
	public get availability(): number {
		return this._availability;
	}

    /**
     * Setter date
     * @param {Date} value
     */
	public set date(value: Date) {
		this._date = value;
	}

    /**
     * Setter availability
     * @param {number} value
     */
	public set availability(value: number) {
		this._availability = value;
	}
    
}