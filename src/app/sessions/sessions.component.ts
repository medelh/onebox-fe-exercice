import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//Services
import { MockService } from '../service/mock.service';
//model
import { EventInfo } from '../model/event-info';
import { Event } from '../model/event';
import { Cart } from '../model/cart';
import { Ticket } from '../model/ticket';

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.css']
})
export class SessionsComponent implements OnInit {
  eventInfo: EventInfo = new EventInfo(new Event(0,"","","","",new Date(),new Date(),""), []);
  arrCart: Cart[] = [];
  cart: Cart = new Cart(new Event(0,"","","","",new Date(),new Date(),""), []);
  error: any = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private mockService: MockService
    ) { }

  ngOnInit(): void {
    this.arrCart = this.mockService.getCart();
    this.activatedRoute.queryParams
    .subscribe(params => {
      let id = params['id'];
      this.getEventInfo(id);
    }
  );
  }

  getEventInfo(id: number){
    this.error.status = 0;
    this.mockService.getEventInfo(id).subscribe(
      (data)=>{
        this.eventInfo = data;
        //this function allows to sort the catalog by endDate in ASC order
        this.eventInfo.sessions.sort((a,b) => a.date > b.date ? 1 : -1);
        //these functions transforms epoch data to Date format
        this.eventInfo.sessions.map((session)=>session.date = new Date(parseInt(session.date.toString())));
        this.getArrCart(id);
      },
      error => {
        this.error = error;
        this.error.statusText = "EVENT INFO NOT FOUND";
      }
    )
  }

  getArrCart(id: number){
    let idFound = false;
    this.arrCart.map((cart)=>{
      if (cart.event.id == id){
        idFound = true;
        this.cart = cart;
        this.setAvailability();
      }
    });
    if (!idFound){
      let arrTickets: Ticket[] = [];
      this.eventInfo.sessions.map((session)=>arrTickets.push(new Ticket(session.date, 0)))
      this.cart.event = this.eventInfo.event;
      this.cart.arr_tickets = arrTickets;
      this.arrCart.push(this.cart);
    }
  }

  setAvailability(){
    for (let i=0; i<this.eventInfo.sessions.length; i++){
      this.eventInfo.sessions[i].availability = this.eventInfo.sessions[i].availability - this.cart.arr_tickets[i].amount;
    }
  }
  dataFromEventAvailability(event: [number, string]) {
    //event[0] contains the position of the array
    //event[1] contains if the client buy or sell tickets
    if (event[1] === "buy"){
      this.eventInfo.sessions[event[0]].availability -= 1;
      this.cart.arr_tickets[event[0]].amount += 1;
    }else if(event[1] === "sell"){
      this.eventInfo.sessions[event[0]].availability += 1;
      this.cart.arr_tickets[event[0]].amount -= 1;
    }
  }

  dataFromShoppingCart(event: [number, string]){
    //event[0] contains the position of the array
    //event[1] contains if the client buy or sell tickets
    if(event[1] === "sell"){
      this.eventInfo.sessions[event[0]].availability += 1;
      this.cart.arr_tickets[event[0]].amount -= 1;
    }
  }

  returnCatalog(){
    this.router.navigate(['/catalog']);
  }

}
