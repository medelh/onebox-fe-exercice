import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http'; 

import { NavbarComponent } from './navbar/navbar.component';
import { CatalogComponent } from './catalog/catalog.component';
import { SessionsComponent } from './sessions/sessions.component';
import { CardComponent } from './card/card.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { EventAvailabilityComponent } from './event-availability/event-availability.component';

const appRoutes: Routes = [
  { path: 'catalog', component: CatalogComponent},
  { path: 'sessions', component: SessionsComponent},
]


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CatalogComponent,
    SessionsComponent,
    CardComponent,
    ShoppingCartComponent,
    EventAvailabilityComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(
      appRoutes, { enableTracing: false, relativeLinkResolution: 'legacy' } 
    ),
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
