import { Component, OnInit } from '@angular/core';
//Services
import { MockService } from '../service/mock.service';
//model
import { Event } from '../model/event';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {
  catalog: Event[] = [];

  constructor(
    private mockService: MockService
) { }

  ngOnInit(): void {
    this.getEvents();
  }

  getEvents(){
    this.mockService.getEvents().subscribe(
      (data)=>{
        this.catalog = data;
        //this function allows to sort the catalog by endDate in ASC order
        this.catalog.sort((a,b) => a.endDate > b.endDate ? 1 : -1);
        //these functions transforms epoch data to Date format
        this.catalog.map((event)=>
        {
          event.startDate = new Date(parseInt(event.startDate.toString()));
          event.endDate = new Date(parseInt(event.endDate.toString()))
        });
      }
    )
  }
}
