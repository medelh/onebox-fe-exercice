import { Injectable } from '@angular/core';

import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

//model
import { Cart } from '../model/cart';

@Injectable({
  providedIn: 'root'
})
export class MockService {
  private eventsJsonURL = 'assets/data/events.json';
  private eventInfo = 'assets/data/event-info-';
  private arrCart: Cart[] = [];
  constructor(private http: HttpClient) { }
  
  public getEvents(): Observable<any> {
    return this.http.get(this.eventsJsonURL);
  }

  public getEventInfo(id: number): Observable<any> {
    let eventInfoJsonURL = this.eventInfo + id.toString() + ".json";
    return this.http.get(eventInfoJsonURL);
  }

  public getCart(){
    return this.arrCart;
  }

  public setCart(arrCart: Cart[]){
    this.arrCart = arrCart;
  }

}
