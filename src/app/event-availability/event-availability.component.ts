import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//model
import { EventInfo } from '../model/event-info';
import { Session } from '../model/session';
import { Event } from '../model/event';
import { Cart } from '../model/cart';

@Component({
  selector: 'app-event-availability',
  templateUrl: './event-availability.component.html',
  styleUrls: ['./event-availability.component.css']
})
export class EventAvailabilityComponent implements OnInit {
  @Input() eventInfo: EventInfo = new EventInfo(new Event(0,"","","","",new Date(),new Date(),""), []);
  @Input() cart: Cart = new Cart(new Event(0,"","","","",new Date(),new Date(),""), []);
  @Input() error: any = [];
  
  @Output() eventEmmitter: EventEmitter<[number, string]> = new EventEmitter<[number, string]>();

  constructor() { }

  ngOnInit(): void {
  }

  lessTickets(i: number){
    this.eventEmmitter.emit([i, "sell"]);
  }

  moreTickets(i: number){
    this.eventEmmitter.emit([i, "buy"]);
  }
}
