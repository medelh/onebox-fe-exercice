import { Component, OnInit, Input } from '@angular/core';
import { Event } from '../model/event';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() event: Event = new Event(0,"","","","",new Date(),new Date(),"");
  constructor(
    private route: Router
    ) { }

  ngOnInit(): void {
  }

  displaySessions(id:number){
    this.route.navigate(['/sessions'], { queryParams: { id: id }});
  }

}
